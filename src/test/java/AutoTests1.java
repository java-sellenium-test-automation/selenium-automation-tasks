import com.github.javafaker.Faker;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import java.io.File;
import java.time.Duration;
import java.util.List;


public class AutoTests1 {

    WebDriver driver;
    Faker fakeData = new Faker();


    //Helper Method
    public void clickSubMenuItem(WebDriver driver , int subMenuItem){
        //Init Js Executor for scroll
        JavascriptExecutor js = (JavascriptExecutor) driver;
        // Init Waiter
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(10));
        WebElement submenuBtn = driver.findElement(By.cssSelector(String.format("div.show>ul>li:nth-child(%d)",subMenuItem)));
        js.executeScript("arguments[0].scrollIntoView(true);",submenuBtn);
        wait.until(ExpectedConditions.elementToBeClickable(submenuBtn));
        submenuBtn.click();
    }




    @BeforeMethod(onlyForGroups = "Alerts/Frame/Windows")
    public void setUpOpenAlerts(){
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.get("https://demoqa.com/");
        driver.manage().timeouts().implicitlyWait((Duration.ofSeconds(10)));
        //Click elements category from main page
        WebElement category = driver.findElement(By.cssSelector("div.category-cards>div:nth-child(3)"));
        category.click();
    }

    @BeforeMethod(onlyForGroups = "Elements")
    public void setUpOpenElements(){
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.get("https://demoqa.com/");
        driver.manage().timeouts().implicitlyWait((Duration.ofSeconds(10)));
        //Click elements category from main page
        WebElement category = driver.findElement(By.cssSelector("div.category-cards>div:nth-child(1)"));
        category.click();

    }

    @BeforeMethod(onlyForGroups = "Interactions")
    public void setUpOpenInteractions(){
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.get("https://demoqa.com/");
        driver.manage().timeouts().implicitlyWait((Duration.ofSeconds(10)));
        //Click elements category from main page
        WebElement category = driver.findElement(By.cssSelector("div.category-cards>div:nth-child(5)"));
        category.click();

    }


    @BeforeMethod(onlyForGroups = "General")
    public void setUpGeneral(){
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.get("https://demoqa.com/");
        driver.manage().timeouts().implicitlyWait((Duration.ofSeconds(10)));
    }

    @Test(testName = "Task1 - Check Page Title",groups = "General")
    public void checkPageTitle(){
        String pageTitleActual = driver.getTitle();
        String pageTitleExpected = "DEMOQA";
        Assert.assertEquals(pageTitleActual,pageTitleExpected);

    }

    @Test(testName = "Task2 - Check if text-box inputs correctly displayed after submit.",groups = "Elements")
    public void checkTextBox() {
        // Init Waiter
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(10));
        //Click TextBox submenu item
        clickSubMenuItem(driver,1);
        //Type random full name to the Full Name field
        String expectedFullName = fakeData.name().fullName();
        driver.findElement(By.cssSelector("input#userName")).sendKeys(expectedFullName);
        //Type random email name to the email field
        String expectedEmail = fakeData.internet().emailAddress();
        driver.findElement(By.cssSelector("input#userEmail")).sendKeys(expectedEmail);
        //Type random address  to the current address field
        String expectedCurrentAddress = fakeData.address().fullAddress();
        driver.findElement(By.cssSelector("textarea#currentAddress")).sendKeys(expectedCurrentAddress);
        //Type random address  to the permanent address field
        String expectedPermanentAddress = fakeData.address().fullAddress();
        driver.findElement(By.cssSelector("textarea#permanentAddress")).sendKeys(expectedPermanentAddress);
        //Init Js Executor for scroll
        JavascriptExecutor js = (JavascriptExecutor) driver;
        //Wait until submit button is clickable
        WebElement submitBtn = driver.findElement(By.cssSelector("button#submit"));
        js.executeScript("arguments[0].scrollIntoView(true);",submitBtn);
        wait.until(ExpectedConditions.elementToBeClickable(submitBtn)).click();
        //Get actual results
        String actualFullName = driver.findElement(By.cssSelector("p#name")).getText().split(":")[1];
        String actualEmail = driver.findElement(By.cssSelector("p#email")).getText().split(":")[1];
        String actualCurrentAddress = driver.findElement(By.cssSelector("p#currentAddress")).getText().split(":")[1];
        String actualPermanentAddress = driver.findElement(By.cssSelector("p#permanentAddress")).getText().split(":")[1];
        //Assert Results
        Assert.assertEquals(actualFullName,expectedFullName);
        Assert.assertEquals(actualEmail,expectedEmail);
        Assert.assertEquals(actualCurrentAddress,expectedCurrentAddress);
        Assert.assertEquals(actualPermanentAddress,expectedPermanentAddress);

    }

    @Test(testName = "Task3 - Check Notes are selected",groups = "Elements")
    public void checkNotes()  {
        //Click Check Box submenu item
        clickSubMenuItem(driver,2);
        //Expand Home Folder
        WebElement expandHomeBtn = driver.findElement(By.cssSelector("button[title='Toggle']"));
        expandHomeBtn.click();
        //Expand Desktop folder
        List<WebElement> expandBtns = driver.findElements(By.cssSelector("button[title='Toggle']"));
        expandBtns.get(1).click();
        //Check Notes
        WebElement notesCheckBox = driver.findElement(By.cssSelector("label[for='tree-node-notes']>span:nth-child(2)>svg"));
        notesCheckBox.click();
        //Assert displayed text
        WebElement textSuccess = driver.findElement(By.cssSelector(".text-success"));
        Assert.assertEquals(textSuccess.getText(),"notes");

    }


    @Test(testName = "Task4 - Check Right Click Me button clicked ",groups = "Elements")
    public void buttonClicked() {
        //Init Action
        Actions action = new Actions(driver);
        //Scroll buttons submenu item into view and click
        clickSubMenuItem(driver,5);
        //Right click to the button
        WebElement rightClkBtn = driver.findElement(By.cssSelector("#rightClickBtn"));
        action.contextClick(rightClkBtn).perform();
        //Assert confirmation text
        WebElement rightClkMsg = driver.findElement(By.cssSelector("#rightClickMessage"));;
        Assert.assertEquals(rightClkMsg.getText(),"You have done a right click");
    }


    @Test(testName = "Task5 - Upload file ",groups = "Elements")
    public void uploadFile()  {
        //Scroll upload and download submenu item into view and click
        clickSubMenuItem(driver,8);
        //Upload file
        WebElement chooseFileBtn = driver.findElement(By.cssSelector("#uploadFile"));
        File file = new File("src/main/data/upload.txt");
        chooseFileBtn.sendKeys(file.getAbsolutePath());
    }


    @Test(testName = "Task6 - Upload file ",groups = "Elements")
    public void buttonVisibility()  {
        WebDriverWait wait =new WebDriverWait(driver,Duration.ofSeconds(5));
        //Scroll dynamic properties submenu item into view and click
        clickSubMenuItem(driver,9);
        //Wait for button to become clickable
        WebElement disabledBtn = driver.findElement(By.cssSelector("button#visibleAfter"));
        wait.until(ExpectedConditions.visibilityOf(disabledBtn));
        disabledBtn.click();
    }

    @Test(testName = "Task7 - Check new window ",groups = "Alerts/Frame/Windows")
    public void checkNewWindow() {
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(5));
        //Scroll browser Windows submenu item into view and click
        clickSubMenuItem(driver, 1);
        //Store ID of original window
        String originalWindow = driver.getWindowHandle();
        //Click new window button
        WebElement newWindowBtn = driver.findElement(By.cssSelector("button#windowButton"));
        newWindowBtn.click();
        //Wait until new window available
        wait.until(ExpectedConditions.numberOfWindowsToBe(2));
        //Switch to new window
        for (String windowHandle : driver.getWindowHandles()) {
            if (!originalWindow.contentEquals(windowHandle)) {
                driver.switchTo().window(windowHandle);
                break;
            }
        }
        //assert text in the new window
        WebElement heading = driver.findElement(By.cssSelector("h1#sampleHeading"));
        Assert.assertEquals(heading.getText(), "This is a sample page");
    }


    @Test(testName = "Task8 - Alert prompt text assertion ",groups = "Alerts/Frame/Windows")
    public void checkAlertAccept() {
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(5));
        //Scroll browser Windows submenu item into view and click
        clickSubMenuItem(driver, 2);
        //Click confirm box will appear button
        WebElement clickMeBtn3 =  driver.findElement(By.cssSelector("button#promtButton"));
        clickMeBtn3.click();
        //Accept Alert
        String text = "Selenium";
        driver.switchTo().alert().sendKeys(text);
        driver.switchTo().alert().accept();
        //Asserting result
        WebElement resultText = driver.findElement(By.cssSelector("span#promptResult"));
        Assert.assertEquals(resultText.getText(),String.format("You entered %s",text));

    }

    @Test(testName = "Task9 - Alert cancel assertion ",groups = "Alerts/Frame/Windows")
    public void checkAlertCancel() {
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(5));
        //Scroll browser Windows submenu item into view and click
        clickSubMenuItem(driver, 2);
        //Click confirm box will appear button
        WebElement clickMeBtn3 =  driver.findElement(By.cssSelector("button#confirmButton"));
        clickMeBtn3.click();
        //Cancel Alert
        driver.switchTo().alert().dismiss();
        //Asserting result
        WebElement resultText = driver.findElement(By.cssSelector("span#confirmResult"));
        Assert.assertEquals(resultText.getText(),"You selected Cancel");


    }



    @Test(testName = "Task10 - Verify Text inside iFrame ",groups = "Alerts/Frame/Windows")
    public void iFrameTextCheck() throws InterruptedException {
        //Scroll buttons submenu item into view and click
        clickSubMenuItem(driver,3);
        //Switch to iFrame
        WebElement iFrame = driver.findElement(By.cssSelector("#frame1"));
        driver.switchTo().frame(iFrame);
        //Assert H1 Text
        WebElement heading = driver.findElement(By.cssSelector("#sampleHeading"));
        Assert.assertEquals(heading.getText(),"This is a sample page");
    }


    @Test(testName = "Task11 - Element is selected ",groups = "Interactions")
    public void checkGridElementSelected(){
        //Scroll buttons submenu item into view and click
        clickSubMenuItem(driver,2);
        //Click grid tab
        WebElement gridTabBtn = driver.findElement(By.cssSelector("a[data-rb-event-key='grid']"));
        gridTabBtn.click();
        //Click grid five
        WebElement fiveBtn = driver.findElement(By.cssSelector("#row2>li:nth-child(2)"));
        fiveBtn.click();
        //Assert Five Selected
        String fiveBtnClassName =  fiveBtn.getAttribute("class");
        Assert.assertTrue(fiveBtnClassName.contains("active"));

    }


    @Test(testName = "Task12 - DragAndDrop ",groups = "Interactions")
    public void dragNdropElement(){
        //Init Action
        Actions action = new Actions(driver);
        //Scroll buttons submenu item into view and click
        clickSubMenuItem(driver,4);
        //Drag and Drop element
        WebElement sourceElm = driver.findElement(By.cssSelector("div#draggable"));
        WebElement destElm = driver.findElement(By.cssSelector("div#droppable"));
        action.dragAndDrop(sourceElm, destElm).build().perform();
        WebElement dropConfirmText = driver.findElement(By.cssSelector("div#droppable>p"));
        Assert.assertEquals(dropConfirmText.getText(),"Dropped!");
    }




    @AfterMethod
    public  void tearDown(){
        driver.quit();
    }

}
